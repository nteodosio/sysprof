# Chinese (China) translation for sysprof.
# Copyright (C) 2020 sysprof's COPYRIGHT HOLDER
# This file is distributed under the same license as the sysprof package.
# Damned <damnedlies@163.com>, 2020.
# Dingzhong Chen <wsxy162@gmail.com>, 2020.
# lumingzh <lumingzh@qq.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: sysprof master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/sysprof/issues\n"
"POT-Creation-Date: 2023-07-31 20:30+0000\n"
"PO-Revision-Date: 2023-08-03 16:39-0400\n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: Chinese - China <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.2.2\n"

#: data/org.gnome.Sysprof.appdata.xml.in.in:5
#: data/org.gnome.Sysprof.desktop.in.in:4 src/sysprof/sysprof-application.c:138
#: src/sysprof/sysprof-application.c:185
#: src/sysprof/sysprof-recording-pad.ui:18
msgid "Sysprof"
msgstr "Sysprof"

#: data/org.gnome.Sysprof.appdata.xml.in.in:6
msgid "Profile an application or entire system"
msgstr "对应用程序或整个系统进行性能分析"

#: data/org.gnome.Sysprof.appdata.xml.in.in:9
msgid "The GNOME Foundation"
msgstr "GNOME 基金会"

#: data/org.gnome.Sysprof.appdata.xml.in.in:12
msgid ""
"Sysprof allows you to profile applications to aid in debugging and "
"optimization."
msgstr "Sysprof 允许您对应用程序进行性能分析，以便调试及优化。"

#: data/org.gnome.Sysprof.desktop.in.in:5
msgid "Profiler"
msgstr "性能分析器"

#: data/org.gnome.Sysprof.desktop.in.in:6
msgid "Profile an application or entire system."
msgstr "对应用程序或整个系统进行性能分析。"

#: src/libsysprof/sysprof-category-summary.c:138
msgid "Uncategorized"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:141
msgid "Accessibility"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:144
#, fuzzy
#| msgid "Functions"
msgid "Actions"
msgstr "函数"

#: src/libsysprof/sysprof-category-summary.c:147
msgid "Crash Handler"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:150
msgid "Context Switches"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:153
msgid "CSS"
msgstr "CSS"

#: src/libsysprof/sysprof-category-summary.c:156
#: src/sysprof/sysprof-window.ui:241
msgid "Graphics"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:159
msgid "Icons"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:162
msgid "Input"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:165
msgid "IO"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:168
msgid "IPC"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:171
msgid "JavaScript"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:174
msgid "Kernel"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:177
msgid "Layout"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:180
msgid "Locking"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:183
msgid "Main Loop"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:186
#: src/sysprof/sysprof-greeter.ui:84
msgid "Memory"
msgstr "内存"

#: src/libsysprof/sysprof-category-summary.c:189
msgid "Paint"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:192
msgid "Unwindable"
msgstr ""

#: src/libsysprof/sysprof-category-summary.c:195
#, fuzzy
#| msgid "Window size"
msgid "Windowing"
msgstr "窗口大小"

#: src/libsysprof/sysprof-document-allocation.c:151
#, fuzzy
#| msgid "All Allocations"
msgid "Allocation"
msgstr "全部分配"

#: src/libsysprof/sysprof-document.c:1160
#: src/libsysprof/sysprof-document.c:1196
msgid "Indexing capture data frames"
msgstr ""

#: src/libsysprof/sysprof-document.c:1374
msgid "Discovering file system mounts"
msgstr ""

#: src/libsysprof/sysprof-document.c:1377
msgid "Discovering process mount namespaces"
msgstr ""

#: src/libsysprof/sysprof-document.c:1380
msgid "Analyzing process address layouts"
msgstr ""

#: src/libsysprof/sysprof-document.c:1383
msgid "Analyzing process command line"
msgstr ""

#: src/libsysprof/sysprof-document.c:1386
msgid "Analyzing file system overlays"
msgstr ""

#: src/libsysprof/sysprof-document.c:1389
#, fuzzy
#| msgid "Processes"
msgid "Processing counters"
msgstr "进程"

#: src/libsysprof/sysprof-document.c:2357
#, fuzzy, c-format
#| msgid "Recording at %s"
msgid "Recording at %X %x"
msgstr "%s 时的记录"

#: src/libsysprof/sysprof-document.c:2359
#, c-format
msgid "Recording at %s"
msgstr "%s 时的记录"

#: src/libsysprof/sysprof-document-dbus-message.c:147
msgid "D-Bus Message"
msgstr "D-Bus 消息"

#: src/libsysprof/sysprof-document-frame.c:139
msgid "Frame"
msgstr ""

#: src/libsysprof/sysprof-document-loader.c:435
msgid "Document loaded"
msgstr ""

#: src/libsysprof/sysprof-document-loader.c:466
#, fuzzy
#| msgid "Recording Failed"
msgid "Loading failed"
msgstr "记录失败"

#: src/libsysprof/sysprof-document-loader.c:478
#: src/libsysprof/sysprof-document-symbols.c:217
#, fuzzy
#| msgid "Stack Traces"
msgid "Symbolizing stack traces"
msgstr "栈轨迹"

#: src/libsysprof/sysprof-document-loader.c:547
#, fuzzy
#| msgid "Loading Processes…"
msgid "Loading document"
msgstr "正在载入进程……"

#: src/libsysprof/sysprof-document-log.c:85
#, fuzzy
#| msgid "Logs"
msgid "Log"
msgstr "日志"

#: src/libsysprof/sysprof-document-mark.c:113
msgid "Mark"
msgstr "标记"

#: src/libsysprof/sysprof-document-process.c:277
#, c-format
msgid "%s [Process %d]"
msgstr ""

#: src/libsysprof/sysprof-document-process.c:279
#, fuzzy, c-format
#| msgid "Processes"
msgid "Process %d"
msgstr "进程"

#: src/libsysprof/sysprof-document-sample.c:136
msgid "Sample"
msgstr ""

#: src/libsysprof/sysprof-system-logs.c:171 src/sysprof/sysprof-greeter.ui:264
msgid "System Logs"
msgstr ""

#: src/libsysprof/sysprof-system-logs.c:172
msgid "Recording system logs is not supported on your platform."
msgstr ""

#: src/libsysprof/sysprof-tracer.c:65
msgid "Tracer"
msgstr ""

#: src/libsysprof/sysprof-tracer.c:66
msgid ""
"Tracing requires spawning a program compiled with ‘-finstrument-functions’. "
"Tracing will not be available."
msgstr ""

#: src/sysprof-cli/sysprof-cli.c:110
msgid "--merge requires at least 2 filename arguments"
msgstr "--merge 至少需要 2 个文件名参数"

#: src/sysprof-cli/sysprof-cli.c:298
#, fuzzy
#| msgid "Disable CPU throttling while profiling"
msgid "Disable CPU throttling while profiling [Deprecated for --power-profile]"
msgstr "在性能分析时禁用 CPU 降频"

#: src/sysprof-cli/sysprof-cli.c:299
#, fuzzy
#| msgid "Make sysprof specific to a task"
msgid "Make sysprof specific to a task [Deprecated]"
msgstr "使 sysprof 特定于一项任务"

#: src/sysprof-cli/sysprof-cli.c:299 src/sysprof/sysprof-frame-utility.ui:109
#: src/sysprof/sysprof-logs-section.ui:179
#: src/sysprof/sysprof-processes-section.ui:282
#: src/sysprof/sysprof-traceables-utility.ui:52
msgid "PID"
msgstr "进程号"

#: src/sysprof-cli/sysprof-cli.c:300
msgid "Run a command and profile the process"
msgstr "运行一个命令并对进程进行性能分析"

#: src/sysprof-cli/sysprof-cli.c:300
msgid "COMMAND"
msgstr "命令"

#: src/sysprof-cli/sysprof-cli.c:301
msgid ""
"Set environment variable for spawned process. Can be used multiple times."
msgstr "为生成的进程设置环境变量。可以多次使用。"

#: src/sysprof-cli/sysprof-cli.c:301
msgid "VAR=VALUE"
msgstr "变量=值"

#: src/sysprof-cli/sysprof-cli.c:302
msgid "Force overwrite the capture file"
msgstr "强制覆盖采集文件"

#: src/sysprof-cli/sysprof-cli.c:303
msgid "Disable recording of battery statistics"
msgstr "禁用电池统计信息记录"

#: src/sysprof-cli/sysprof-cli.c:304
msgid "Disable recording of CPU statistics"
msgstr "禁用 CPU 统计信息记录"

#: src/sysprof-cli/sysprof-cli.c:305
msgid "Disable recording of Disk statistics"
msgstr "禁用磁盘统计信息记录"

#: src/sysprof-cli/sysprof-cli.c:306
msgid "Do not record stacktraces using Linux perf"
msgstr "不要使用 Linux perf 记录栈轨迹"

#: src/sysprof-cli/sysprof-cli.c:307
msgid "Do not append symbol name information from local machine"
msgstr "不要附加来自本地计算机的符号名称信息"

#: src/sysprof-cli/sysprof-cli.c:308
msgid "Disable recording of memory statistics"
msgstr "禁用内存统计信息记录"

#: src/sysprof-cli/sysprof-cli.c:309
msgid "Disable recording of network statistics"
msgstr "禁用网络统计信息记录"

#: src/sysprof-cli/sysprof-cli.c:310
msgid "Set SYSPROF_TRACE_FD environment for subprocess"
msgstr "为子进程设置 SYSPROF_TRACE_FD 环境"

#: src/sysprof-cli/sysprof-cli.c:311
#, fuzzy
#| msgid "Connect to the session bus"
msgid "Profile the D-Bus session bus"
msgstr "连接到会话总线"

#: src/sysprof-cli/sysprof-cli.c:312
#, fuzzy
#| msgid "Profile the system"
msgid "Profile the D-Bus system bus"
msgstr "对系统进行性能分析"

#: src/sysprof-cli/sysprof-cli.c:313
msgid "Set GJS_TRACE_FD environment to trace GJS processes"
msgstr "设置 GJS_TRACE_FD 环境以跟踪 GJS 进程"

#: src/sysprof-cli/sysprof-cli.c:314
msgid "Set GTK_TRACE_FD environment to trace a GTK application"
msgstr "设置 GTK_TRACE_FD 环境以跟踪 GTK 应用程序"

#: src/sysprof-cli/sysprof-cli.c:315
msgid "Include RAPL energy statistics"
msgstr "包含 RAPL 电量统计信息"

#: src/sysprof-cli/sysprof-cli.c:316
msgid "Profile memory allocations and frees"
msgstr "对内存分配和释放进行性能分析"

#: src/sysprof-cli/sysprof-cli.c:317
msgid "Connect to org.gnome.Shell for profiler statistics"
msgstr "连接到 org.gnome.Shell 以获取性能分析器统计信息"

#: src/sysprof-cli/sysprof-cli.c:318
msgid "Track performance of the applications main loop"
msgstr "跟踪应用程序主循环的性能"

#: src/sysprof-cli/sysprof-cli.c:320
msgid "Merge all provided *.syscap files and write to stdout"
msgstr "合并所有提供的 *.syscap 文件并写到标准输出"

#: src/sysprof-cli/sysprof-cli.c:321
msgid "Print the sysprof-cli version and exit"
msgstr "打印 sysprof-cli 的版本并退出"

#: src/sysprof-cli/sysprof-cli.c:354
msgid "[CAPTURE_FILE] [-- COMMAND ARGS] — Sysprof"
msgstr "[采集文件] [-- 命令参数] — Sysprof"

#: src/sysprof-cli/sysprof-cli.c:357
msgid ""
"\n"
"Examples:\n"
"\n"
"  # Record gtk4-widget-factory using trace-fd to get application provided\n"
"  # data as well as GTK and GNOME Shell data providers\n"
"  sysprof-cli --gtk --gnome-shell --use-trace-fd -- gtk4-widget-factory\n"
"\n"
"  # Merge multiple syscap files into one\n"
"  sysprof-cli --merge a.syscap b.syscap > c.syscap\n"
msgstr ""
"\n"
"示例：\n"
"\n"
"  # 使用 trace-fd 记录 gtk4-widget-factory 以获取应用程序提供的\n"
"  # 数据以及 GTK 和 GNOME Shell 数据提供者\n"
"  sysprof-cli --gtk --gnome-shell --use-trace-fd -- gtk4-widget-factory\n"
"\n"
"  # 将多个 syscap 文件合并为一个\n"
"  sysprof-cli --merge a.syscap b.syscap > c.syscap\n"

#: src/sysprof-cli/sysprof-cli.c:393
msgid "Too many arguments were passed to sysprof-cli:"
msgstr "向 sysprof-cli 传递的参数过多："

#. Translators: %s is a file name.
#: src/sysprof-cli/sysprof-cli.c:440
#, c-format
msgid "%s exists. Use --force to overwrite\n"
msgstr "%s 已存在。使用 --force 进行覆盖\n"

#: src/sysprofd/org.gnome.sysprof3.policy.in:13
msgid "Profile the system"
msgstr "对系统进行性能分析"

#: src/sysprofd/org.gnome.sysprof3.policy.in:14
msgid "Authentication is required to profile the system."
msgstr "需要授权才能对系统进行性能分析。"

#: src/sysprof/gtk/help-overlay.ui:10
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: src/sysprof/gtk/help-overlay.ui:13
#, fuzzy
#| msgid "Help"
msgctxt "shortcut window"
msgid "Show Help"
msgstr "帮助"

#: src/sysprof/gtk/help-overlay.ui:19
#, fuzzy
#| msgid "Keyboard Shortcuts"
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "键盘快捷键"

#: src/sysprof/gtk/help-overlay.ui:25
#, fuzzy
#| msgid "New Window"
msgctxt "shortcut window"
msgid "Close Window"
msgstr "新建窗口"

#: src/sysprof/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: src/sysprof/gtk/menus.ui:6
msgid "_Preferences"
msgstr "首选项(_P)"

#: src/sysprof/gtk/menus.ui:10
msgid "_Keyboard Shortcuts"
msgstr "键盘快捷键(_K)"

#: src/sysprof/gtk/menus.ui:14
msgid "_Help"
msgstr "帮助(_H)"

#: src/sysprof/gtk/menus.ui:18
msgid "_About Sysprof"
msgstr "关于 Sysprof(_A)"

#: src/sysprof/sysprof-application.c:147
msgid "A system profiler"
msgstr "系统性能分析器"

#: src/sysprof/sysprof-application.c:148
msgid "translator-credits"
msgstr "Damned <damnedlies@163.com>, 2020."

#: src/sysprof/sysprof-callgraph-view.ui:22
msgid "Functions"
msgstr "函数"

#: src/sysprof/sysprof-callgraph-view.ui:71
#, fuzzy
#| msgid "Functions"
msgid "Filter Functions"
msgstr "函数"

#: src/sysprof/sysprof-callgraph-view.ui:94
msgid "Callers"
msgstr "调用者"

#: src/sysprof/sysprof-callgraph-view.ui:146
msgid "Descendants"
msgstr "后代"

#: src/sysprof/sysprof-counters-section.ui:26
#: src/sysprof/sysprof-cpu-section.ui:169
#: src/sysprof/sysprof-energy-section.ui:177
#: src/sysprof/sysprof-graphics-section.ui:88
#: src/sysprof/sysprof-network-section.ui:86
#: src/sysprof/sysprof-storage-section.ui:86
#, fuzzy
#| msgid "Counters Captured"
msgid "Counters Chart"
msgstr "已采集的计数器"

#: src/sysprof/sysprof-counters-section.ui:40
#: src/sysprof/sysprof-cpu-section.ui:323
#: src/sysprof/sysprof-energy-section.ui:312
#: src/sysprof/sysprof-graphics-section.ui:223
#: src/sysprof/sysprof-network-section.ui:291
#: src/sysprof/sysprof-storage-section.ui:291
#, fuzzy
#| msgid "Counters"
msgid "Counters Table"
msgstr "计数器"

#: src/sysprof/sysprof-cpu-section.ui:378
#: src/sysprof/sysprof-dbus-section.ui:105
#: src/sysprof/sysprof-energy-section.ui:348
#: src/sysprof/sysprof-frame-utility.ui:49
#: src/sysprof/sysprof-graphics-section.ui:259
#: src/sysprof/sysprof-logs-section.ui:110
#: src/sysprof/sysprof-metadata-section.ui:38
#: src/sysprof/sysprof-network-section.ui:346
#: src/sysprof/sysprof-process-dialog.ui:64
#: src/sysprof/sysprof-processes-section.ui:214
#: src/sysprof/sysprof-storage-section.ui:346
#: src/sysprof/sysprof-traceables-utility.ui:23
msgid "Time"
msgstr "时间"

#: src/sysprof/sysprof-cpu-section.ui:412
#: src/sysprof/sysprof-energy-section.ui:382
#: src/sysprof/sysprof-graphics-section.ui:293
#: src/sysprof/sysprof-marks-section.ui:237
#: src/sysprof/sysprof-network-section.ui:380
#: src/sysprof/sysprof-storage-section.ui:380
msgid "Category"
msgstr ""

#: src/sysprof/sysprof-cpu-section.ui:450
#: src/sysprof/sysprof-energy-section.ui:420
#: src/sysprof/sysprof-graphics-section.ui:331
#: src/sysprof/sysprof-marks-section.ui:271
#: src/sysprof/sysprof-network-section.ui:418
#: src/sysprof/sysprof-storage-section.ui:418
msgid "Name"
msgstr ""

#: src/sysprof/sysprof-cpu-section.ui:488
#: src/sysprof/sysprof-energy-section.ui:458
#: src/sysprof/sysprof-graphics-section.ui:369
#: src/sysprof/sysprof-metadata-section.ui:105
#: src/sysprof/sysprof-network-section.ui:456
#: src/sysprof/sysprof-storage-section.ui:456
msgid "Value"
msgstr ""

#: src/sysprof/sysprof-cpu-section.ui:529
#, fuzzy
#| msgid "Info"
msgid "CPU Info"
msgstr "信息"

#: src/sysprof/sysprof-cpu-section.ui:560
#: src/sysprof/sysprof-process-dialog.ui:248
#, fuzzy
#| msgid "PID"
msgid "ID"
msgstr "进程号"

#: src/sysprof/sysprof-cpu-section.ui:595
msgid "Core"
msgstr ""

#: src/sysprof/sysprof-cpu-section.ui:630
msgid "Model Name"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:139
msgid "Bus"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:178
msgid "Serial"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:215
msgid "Reply"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:252
msgid "Flags"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:281
#: src/sysprof/sysprof-files-section.ui:107
msgid "Size"
msgstr "大小"

#: src/sysprof/sysprof-dbus-section.ui:317
#: src/sysprof/sysprof-frame-utility.ui:20
#: src/sysprof/sysprof-mark-table.ui:221
msgid "Type"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:346
msgid "Sender"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:380
#, fuzzy
#| msgid "Duration"
msgid "Destination"
msgstr "持续时间"

#: src/sysprof/sysprof-dbus-section.ui:414
#: src/sysprof/sysprof-files-section.ui:38
msgid "Path"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:448
msgid "Interface"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:482
msgid "Member"
msgstr ""

#: src/sysprof/sysprof-dbus-section.ui:516
#, fuzzy
#| msgid "Capture"
msgid "Signature"
msgstr "采集"

#: src/sysprof/sysprof-files-section.ui:4
#, fuzzy
#| msgctxt "shortcut window"
#| msgid "Files"
msgid "Files"
msgstr "文件"

#: src/sysprof/sysprof-files-section.ui:73
msgid "Compressed"
msgstr ""

#: src/sysprof/sysprof-frame-utility.ui:80
#: src/sysprof/sysprof-logs-section.ui:143
#: src/sysprof/sysprof-mark-table.ui:116
#: src/sysprof/sysprof-traceables-utility.ui:81
msgid "CPU"
msgstr ""

#: src/sysprof/sysprof-greeter.c:219 src/sysprof/sysprof-greeter.c:284
msgid "Must Capture to Local File"
msgstr ""

#: src/sysprof/sysprof-greeter.c:220 src/sysprof/sysprof-greeter.c:285
msgid "You must choose a local file to capture using Sysprof"
msgstr ""

#: src/sysprof/sysprof-greeter.c:221 src/sysprof/sysprof-greeter.c:286
#: src/sysprof/sysprof-recording-pad.c:115 src/sysprof/sysprof-window.c:523
msgid "Close"
msgstr "关闭"

#: src/sysprof/sysprof-greeter.c:242
#, c-format
msgid "System Capture from %s.syscap"
msgstr ""

#: src/sysprof/sysprof-greeter.c:246
#, fuzzy
#| msgid "Recording Failed"
msgid "Record to File"
msgstr "记录失败"

#: src/sysprof/sysprof-greeter.c:247 src/sysprof/sysprof-greeter.ui:27
#, fuzzy
#| msgid "_Record"
msgid "Record"
msgstr "记录 (_R)"

#: src/sysprof/sysprof-greeter.c:305 src/sysprof/sysprof-greeter.ui:311
#, fuzzy
#| msgctxt "shortcut window"
#| msgid "Open recording"
msgid "Open Recording"
msgstr "打开记录"

#: src/sysprof/sysprof-greeter.c:306 src/sysprof/sysprof-greeter.ui:306
msgid "Open"
msgstr "打开"

#: src/sysprof/sysprof-greeter.c:310
#, fuzzy
#| msgid "Sysprof Captures"
msgid "Sysprof Capture (*.syscap)"
msgstr "Sysprof 采集文件"

#: src/sysprof/sysprof-greeter.ui:33 src/sysprof/sysprof-samples-section.ui:4
#, fuzzy
#| msgid "Profiler"
msgid "Time Profiler"
msgstr "性能分析器"

#: src/sysprof/sysprof-greeter.ui:37
msgid "Sample Native Stacks"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:38
#, fuzzy
#| msgid "Do not record stacktraces using Linux perf"
msgid "Record native stack traces using a sampling profiler"
msgstr "不要使用 Linux perf 记录栈轨迹"

#: src/sysprof/sysprof-greeter.ui:49
#, fuzzy
#| msgid "Performance"
msgid "Performance Profile"
msgstr "性能"

#: src/sysprof/sysprof-greeter.ui:50
msgid "Hold the performance profile for the duration of the recording"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:60
msgid "Sample JavaScript Stacks"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:61
#, fuzzy
#| msgid "Do not record stacktraces using Linux perf"
msgid "Record JavaScript stack traces using a sampling profiler"
msgstr "不要使用 Linux perf 记录栈轨迹"

#: src/sysprof/sysprof-greeter.ui:71
msgid "This feature is only supported when launching a GJS-based application."
msgstr ""

#: src/sysprof/sysprof-greeter.ui:88
#, fuzzy
#| msgid "Memory Usage"
msgid "System Memory Usage"
msgstr "内存用量"

#: src/sysprof/sysprof-greeter.ui:89
msgid "Record coarse-grained counters about system memory usage"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:105
#, fuzzy
#| msgid "Memory Allocations"
msgid "Trace Memory Allocations"
msgstr "内存分配"

#: src/sysprof/sysprof-greeter.ui:106
msgid ""
"Record a strack trace when <tt>malloc</tt> or similar functions are used"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:116
msgid "Memory tracing is only supported when launching an application."
msgstr ""

#: src/sysprof/sysprof-greeter.ui:129
#, fuzzy
#| msgid "Display supplemental graphs"
msgid "Display &amp; Graphics"
msgstr "显示补充图表"

#: src/sysprof/sysprof-greeter.ui:133
msgid "Compositor Frame Timings"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:134
msgid "Record frame-timing information from the GNOME Shell compositor"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:150
#, fuzzy
#| msgid "Launch Application"
msgid "Detect Application Hangs"
msgstr "启动应用程序"

#: src/sysprof/sysprof-greeter.ui:151
#, fuzzy
#| msgid "Track performance of the applications main loop"
msgid "Detect hangs in the application main loop"
msgstr "跟踪应用程序主循环的性能"

#: src/sysprof/sysprof-greeter.ui:161
msgid ""
"Hang detection is only supported when launching a GTK-based application."
msgstr ""

#: src/sysprof/sysprof-greeter.ui:174
#, fuzzy
#| msgid "Profiler"
msgid "D-Bus Profiler"
msgstr "性能分析器"

#: src/sysprof/sysprof-greeter.ui:178
msgid "D-Bus Session Bus"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:179
msgid "Record messages on the D-Bus user session bus"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:190
msgid "D-Bus System Bus"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:191
msgid "Record messages on the D-Bus system bus"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:203
msgid "Counters"
msgstr "计数器"

#: src/sysprof/sysprof-greeter.ui:207
msgid "Disk Usage"
msgstr "磁盘占用"

#: src/sysprof/sysprof-greeter.ui:208
msgid "Record coarse-grained counters about storage throughput"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:220
#, fuzzy
#| msgid "Memory Usage"
msgid "Network Usage"
msgstr "内存用量"

#: src/sysprof/sysprof-greeter.ui:221
msgid "Record coarse-grained counters about network traffic"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:233
msgid "Energy Usage"
msgstr "电量用量"

#: src/sysprof/sysprof-greeter.ui:234
msgid "Record coarse-grained counters about energy usage in Watts"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:246
msgid "Battery Charge"
msgstr "电池电量"

#: src/sysprof/sysprof-greeter.ui:247
msgid "Record coarse-grained counters about battery charge or discharge rates"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:260
msgid "System"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:265
msgid "Watch the system log for new messages and record them"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:277
msgid "Bundle Symbols"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:278
msgid "Make recording shareable by bundling decoded symbols"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:296
msgid "Recent"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:315
#, fuzzy
#| msgid "Capture"
msgid "Capture File"
msgstr "采集"

#: src/sysprof/sysprof-greeter.ui:337
msgid "Symbols"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:341
msgid "Ignore Bundled Symbols"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:342
msgid "Do not use embedded symbols that were resolved on the capture system"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:357
msgid "Override Kernel Symbols"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:358
msgid "Specify a “kallsyms” file overriding what is bundled with a recording"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:374
msgid "Symbol Directories"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:377
msgid "Additional symbol directories to use when symbolizing stack traces."
msgstr ""

#: src/sysprof/sysprof-greeter.ui:388
msgid "Add Symbol Directory"
msgstr ""

#: src/sysprof/sysprof-greeter.ui:412
#, fuzzy
#| msgid "Recording…"
msgid "Record to _File…"
msgstr "正在记录……"

#: src/sysprof/sysprof-greeter.ui:420
msgid "_Record to Memory"
msgstr ""

#: src/sysprof/sysprof-logs-section.c:95
msgid "Critical"
msgstr "严重错误"

#: src/sysprof/sysprof-logs-section.c:98
msgid "Warning"
msgstr "警告"

#: src/sysprof/sysprof-logs-section.c:101
msgid "Debug"
msgstr "调试"

#: src/sysprof/sysprof-logs-section.c:104
#: src/sysprof/sysprof-logs-section.ui:286
msgid "Message"
msgstr "消息"

#: src/sysprof/sysprof-logs-section.c:107
msgid "Info"
msgstr "信息"

#: src/sysprof/sysprof-logs-section.c:110
msgid "Error"
msgstr "错误"

#: src/sysprof/sysprof-logs-section.ui:4
msgid "Logs"
msgstr "日志"

#: src/sysprof/sysprof-logs-section.ui:215
msgid "Severity"
msgstr "严重程度"

#: src/sysprof/sysprof-logs-section.ui:251
msgid "Domain"
msgstr "域"

#: src/sysprof/sysprof-marks-section.ui:4
msgid "Marks"
msgstr "标记"

#: src/sysprof/sysprof-marks-section.ui:73
#, fuzzy
#| msgid "Marks Captured"
msgid "Mark Chart"
msgstr "已采集的标记"

#: src/sysprof/sysprof-marks-section.ui:86
msgid "Mark Table"
msgstr ""

#: src/sysprof/sysprof-marks-section.ui:99
msgid "Mark Waterfall"
msgstr ""

#: src/sysprof/sysprof-marks-section.ui:201
msgid "Summary"
msgstr "概要"

#: src/sysprof/sysprof-marks-section.ui:306
msgid "Minimum"
msgstr ""

#: src/sysprof/sysprof-marks-section.ui:340
msgid "Maximum"
msgstr ""

#: src/sysprof/sysprof-marks-section.ui:374
msgid "Average"
msgstr ""

#: src/sysprof/sysprof-marks-section.ui:408
msgid "Median"
msgstr ""

#: src/sysprof/sysprof-mark-table.ui:47
msgid "Start"
msgstr ""

#: src/sysprof/sysprof-mark-table.ui:81
#: src/sysprof/sysprof-processes-section.ui:248
msgid "Duration"
msgstr "持续时间"

#: src/sysprof/sysprof-mark-table.ui:151
msgid "Process"
msgstr "进程"

#: src/sysprof/sysprof-mark-table.ui:186
msgid "Group"
msgstr "组"

#: src/sysprof/sysprof-mark-table.ui:256
msgid "Description"
msgstr "描述"

#: src/sysprof/sysprof-memory-callgraph-view.ui:8
#: src/sysprof/sysprof-memory-callgraph-view.ui:75
#: src/sysprof/sysprof-memory-callgraph-view.ui:142
#: src/sysprof/sysprof-weighted-callgraph-view.ui:8
#: src/sysprof/sysprof-weighted-callgraph-view.ui:105
#: src/sysprof/sysprof-weighted-callgraph-view.ui:172
msgid "Self"
msgstr "自身"

#: src/sysprof/sysprof-memory-callgraph-view.ui:38
#: src/sysprof/sysprof-memory-callgraph-view.ui:105
#: src/sysprof/sysprof-memory-callgraph-view.ui:172
#: src/sysprof/sysprof-weighted-callgraph-view.ui:38
#: src/sysprof/sysprof-weighted-callgraph-view.ui:135
#: src/sysprof/sysprof-weighted-callgraph-view.ui:202
msgid "Total"
msgstr "总计"

#: src/sysprof/sysprof-memory-section.ui:4
msgid "Memory Allocations"
msgstr "内存分配"

#: src/sysprof/sysprof-memory-section.ui:126
#: src/sysprof/sysprof-samples-section.ui:143
#: src/sysprof/sysprof-traceables-utility.ui:162
#, fuzzy
#| msgid "Stack Traces"
msgid "Stack Trace"
msgstr "栈轨迹"

#: src/sysprof/sysprof-metadata-section.ui:4
msgid "Metadata"
msgstr ""

#: src/sysprof/sysprof-metadata-section.ui:71
msgid "Key"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:35
msgid "Address Layout"
msgstr "地址布局"

#: src/sysprof/sysprof-process-dialog.ui:97
msgid "Start Address"
msgstr "开始地址"

#: src/sysprof/sysprof-process-dialog.ui:137
msgid "End Address"
msgstr "结束地址"

#: src/sysprof/sysprof-process-dialog.ui:177
msgid "File"
msgstr "文件"

#: src/sysprof/sysprof-process-dialog.ui:219
#, fuzzy
#| msgid "Counters"
msgid "Mounts"
msgstr "计数器"

#: src/sysprof/sysprof-process-dialog.ui:282
msgid "Parent"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:316
msgid "Major"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:350
msgid "Minor"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:384
msgid "Mount Point"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:418
msgid "Mount Source"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:452
msgid "Root"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:486
#, fuzzy
#| msgctxt "shortcut window"
#| msgid "Files"
msgid "Filesystem"
msgstr "文件"

#: src/sysprof/sysprof-process-dialog.ui:520
#, fuzzy
#| msgid "Functions"
msgid "Options"
msgstr "函数"

#: src/sysprof/sysprof-process-dialog.ui:562
#, fuzzy
#| msgid "Reads"
msgid "Threads"
msgstr "读取"

#: src/sysprof/sysprof-process-dialog.ui:591
msgid "Thread ID"
msgstr ""

#: src/sysprof/sysprof-process-dialog.ui:625
msgid "Main Thread"
msgstr ""

#: src/sysprof/sysprof-processes-section.ui:4
msgid "Processes"
msgstr "进程"

#: src/sysprof/sysprof-processes-section.ui:73
#, fuzzy
#| msgid "Processes Captured"
msgid "Process Chart"
msgstr "已采集的进程"

#: src/sysprof/sysprof-processes-section.ui:173
#, fuzzy
#| msgid "Processes"
msgid "Process Table"
msgstr "进程"

#: src/sysprof/sysprof-processes-section.ui:316
msgid "Command Line"
msgstr "命令行"

#. translators: this expands to the number of events recorded by the profiler as an indicator of progress
#: src/sysprof/sysprof-recording-pad.c:56
msgid "%"
msgstr ""

#: src/sysprof/sysprof-recording-pad.c:111
msgid "Recording Failed"
msgstr "记录失败"

#: src/sysprof/sysprof-recording-pad.c:113
#, fuzzy, c-format
#| msgid "Sysprof failed to parse offset for “%s”."
msgid ""
"Sysprof failed to record.\n"
"\n"
"%s"
msgstr "Sysprof 无法解析“%s”的偏移量。"

#: src/sysprof/sysprof-recording-pad.ui:69
#, fuzzy
#| msgid "_Stop Recording"
msgid "Stop Recording"
msgstr "停止记录 (_S)"

#: src/sysprof/sysprof-sidebar.ui:30 src/sysprof/sysprof-window.ui:98
msgid "Seek Backward"
msgstr ""

#: src/sysprof/sysprof-sidebar.ui:40 src/sysprof/sysprof-window.ui:108
#, fuzzy
#| msgctxt "shortcut window"
#| msgid "Zoom out"
msgid "Zoom Out"
msgstr "缩小"

#: src/sysprof/sysprof-sidebar.ui:50 src/sysprof/sysprof-window.ui:118
#, fuzzy
#| msgctxt "shortcut window"
#| msgid "Reset zoom"
msgid "Reset Zoom"
msgstr "重置缩放"

#: src/sysprof/sysprof-sidebar.ui:61 src/sysprof/sysprof-window.ui:128
#, fuzzy
#| msgctxt "shortcut window"
#| msgid "Zoom in"
msgid "Zoom In"
msgstr "放大"

#: src/sysprof/sysprof-sidebar.ui:71 src/sysprof/sysprof-window.ui:138
msgid "Seek Forward"
msgstr ""

#: src/sysprof/sysprof-traceables-utility.ui:109
msgid "Depth"
msgstr ""

#: src/sysprof/sysprof-weighted-callgraph-view.ui:68
msgid "Hits"
msgstr "采样数"

#: src/sysprof/sysprof-window.c:519
msgid "Invalid Document"
msgstr ""

#: src/sysprof/sysprof-window.c:521
#, c-format
msgid ""
"The document could not be loaded. Please check that you have the correct "
"capture file.\n"
"\n"
"%s"
msgstr ""

#: src/sysprof/sysprof-window.ui:211
msgid "D-Bus"
msgstr "D-Bus"

#: src/sysprof/sysprof-window.ui:221
msgid "CPU Usage"
msgstr "CPU 使用率"

#: src/sysprof/sysprof-window.ui:231
msgid "Energy"
msgstr "能量"

#: src/sysprof/sysprof-window.ui:251
msgid "Network"
msgstr "网络"

#: src/sysprof/sysprof-window.ui:261
msgid "Storage"
msgstr "存储"

#: src/sysprof/sysprof-window.ui:306
#, fuzzy
#| msgid "Record Again"
msgid "_Record Again…"
msgstr "再次记录"

#: src/sysprof/sysprof-window.ui:310
msgid "Open Recording…"
msgstr "打开记录…"

#: src/sysprof/sysprof-window.ui:314
msgid "Save As…"
msgstr "另存为…"

#: src/sysprof/sysprof-window.ui:319
msgid "Preferences"
msgstr "首选项"

#: src/sysprof/sysprof-window.ui:324
msgid "Help"
msgstr "帮助"

#: src/sysprof/sysprof-window.ui:329
msgid "About Sysprof"
msgstr "关于 Sysprof"

#~ msgid "Sysprof was unable to generate a callgraph from the system capture."
#~ msgstr "Sysprof 无法由系统采样生成调用图。"

#, c-format
#~ msgid "Sysprof failed to find field “%s”."
#~ msgstr "Sysprof 无法找到字段“%s”。"

#, c-format
#~ msgid "Sysprof failed to get perf_event ID."
#~ msgstr "Sysprof 无法获取 perf_event ID。"

#, c-format
#~ msgid "An error occurred while attempting to access performance counters"
#~ msgstr "尝试访问性能计数器时发生了错误"

#~ msgid "Battery Charge (All)"
#~ msgstr "电池电量（全部）"

#~ msgid "Battery"
#~ msgstr "电池"

#~ msgid "Stack Traces (In Kernel)"
#~ msgstr "栈轨迹（内核中）"

#~ msgid "Stack Traces (In User)"
#~ msgstr "栈轨迹（用户中）"

#~ msgid "Callgraph"
#~ msgstr "调用图"

#~ msgid "Generating Callgraph"
#~ msgstr "生成调用图"

#~ msgid "Sysprof is busy creating the selected callgraph."
#~ msgstr "Sysprof 正在创建选定的调用图。"

#~ msgid "Not Enough Samples"
#~ msgstr "样本不足"

#~ msgid "More samples are necessary to display a callgraph."
#~ msgstr "需要更多样本才能显示调用图。"

#, c-format
#~ msgctxt "progress bar label"
#~ msgid "%d %%"
#~ msgstr "%d %%"

#~ msgid "CPU Frequency"
#~ msgstr "CPU 频率"

#~ msgid "CPU Frequency (All)"
#~ msgstr "CPU 频率（全部）"

#~ msgid "CPU Usage (All)"
#~ msgstr "CPU 使用率（全部）"

#~ msgid "Memory Capture"
#~ msgstr "内存采集"

#, c-format
#~ msgid "%0.4lf seconds"
#~ msgstr "%0.4lf 秒"

#~| msgid "Duration"
#~ msgid "Location"
#~ msgstr "位置"

#~| msgid "Record Again"
#~ msgid "Recorded At"
#~ msgstr "记录于"

#~ msgid "CPU Model"
#~ msgstr "CPU 型号"

#~ msgid "Statistics"
#~ msgstr "统计信息"

#~ msgid "Number of stack traces sampled for performance callgraphs"
#~ msgstr "为性能调用图而采样的栈轨迹数量"

#~ msgid "Number of marks seen"
#~ msgstr "看到的标记数"

#~ msgid "Number of processes seen"
#~ msgstr "看到的进程数"

#~ msgid "Forks"
#~ msgstr "分叉"

#~ msgid "Number of times a process forked"
#~ msgstr "进程分叉次数"

#~ msgid "Number of stack traces recorded for tracing memory allocations"
#~ msgstr "为跟踪内存分配而记录的栈轨迹数量"

#~ msgid "Number of recorded counter values"
#~ msgstr "记录的计数器值的数量"

#~ msgid "Min"
#~ msgstr "最小"

#~ msgid "Max"
#~ msgstr "最大"

#~ msgid "Avg"
#~ msgstr "平均"

#~ msgid "Disk"
#~ msgstr "磁盘"

#~ msgid "Writes"
#~ msgstr "写入"

#~ msgid "New Recording"
#~ msgstr "新建记录"

#~ msgid "The recording could not be opened"
#~ msgstr "无法打开记录"

#, c-format
#~ msgid "Failed to save recording: %s"
#~ msgstr "无法保存记录：%s"

#~ msgid "Save Recording"
#~ msgstr "保存记录"

#~ msgid "Save"
#~ msgstr "保存"

#~ msgid "Cancel"
#~ msgstr "取消"

#~ msgid "Details"
#~ msgstr "详细信息"

#~ msgid "Remove environment variable"
#~ msgstr "移除环境变量"

#~| msgid "Remove environment variable"
#~ msgid "New environment variable…"
#~ msgstr "新建环境变量……"

#~ msgid "Ouch, that hurt!"
#~ msgstr "出错了！"

#~ msgid ""
#~ "Something unexpectedly went wrong while trying to profile your system."
#~ msgstr "尝试对您的系统进行性能分析时发生了意外错误。"

#~ msgid "Timings"
#~ msgstr "定时"

#~ msgid "End"
#~ msgstr "结束"

#~ msgid "No Timings Available"
#~ msgstr "无可用定时"

#~ msgid "No timing data was found for the current selection"
#~ msgstr "未找到当前选择项的定时数据"

#~ msgid "Track Allocations"
#~ msgstr "跟踪分配"

#, c-format
#~ msgid "> %s to %s"
#~ msgstr "> %s 到 %s"

#~| msgid "Memory Allocations"
#~ msgid "Number of Allocations"
#~ msgstr "分配数量"

#~| msgid "Profile memory allocations and frees"
#~ msgid "Total number of allocation and free records"
#~ msgstr "分配和释放记录的总数"

#~ msgid "Leaked Allocations"
#~ msgstr "泄漏分配"

#~ msgid "Number of allocations without a free record"
#~ msgstr "没有释放记录的分配数量"

#~ msgid "Temporary Allocations"
#~ msgstr "临时分配"

#~ msgid "Number of allocations freed from similar stack trace"
#~ msgstr "从类似的栈轨迹释放的分配数量"

#~| msgid "Allocations Captured"
#~ msgid "Allocations by Size"
#~ msgstr "分配大小"

#~ msgid "Analyzing Memory Allocations"
#~ msgstr "分析内存分配"

#~ msgid "Sysprof is busy analyzing memory allocations."
#~ msgstr "Sysprof 正在分析内存分配。"

#~ msgid "Memory Used"
#~ msgstr "已用内存"

#~ msgid "GNOME Shell"
#~ msgstr "GNOME Shell"

#~ msgid "Profiling Target"
#~ msgstr "性能分析目标"

#~| msgid "Profile the system"
#~ msgid "Profile Entire System"
#~ msgstr "对整个系统进行性能分析"

#~| msgid "Sysprof was unable to generate a callgraph from the system capture."
#~ msgid ""
#~ "Sysprof can generate callgraphs for one or more processes on your system."
#~ msgstr "Sysprof 可以为您的系统上的一个或多个进程生成调用图。"

#~| msgid "Search Processes…"
#~ msgid "Search Processes"
#~ msgstr "搜索进程"

#~ msgid ""
#~ "Sysprof can launch an application to be profiled. The profiler will "
#~ "automatically stop when it exits."
#~ msgstr ""
#~ "Sysprof 可以启动要进行性能分析的应用程序。性能分析器将在它退出时自动停止。"

#~ msgid "Inherit Environment"
#~ msgstr "继承环境"

#~ msgid ""
#~ "Enable to ensure your application shares the display, message-bus, and "
#~ "other desktop environment settings."
#~ msgstr ""
#~ "启用此选项以保证您的应用程序共享显示、消息总线，以及其他桌面环境设置。"

#~ msgid "Allow CPU Throttling"
#~ msgstr "允许 CPU 降频"

#~ msgid ""
#~ "When enabled, your system is allowed to scale CPU frequency as necessary."
#~ msgstr "启用时，您的系统可以根据需要调整 CPU 频率。"

#~ msgid "Instruments"
#~ msgstr "仪表"

#~ msgid ""
#~ "Track application memory allocations (Sysprof must launch target "
#~ "application)"
#~ msgstr "跟踪应用程序的内存分配（Sysprof 必须启动目标应用程序）"

#~ msgid "Track slow operations on your applications main loop"
#~ msgstr "跟踪您的应用程序主循环上的慢速操作"

#~ msgid "Energy Usage (All)"
#~ msgstr "电量用量（全部）"

#~ msgid ""
#~ "Did you know you can use <a href=\"help:sysprof\">sysprof-cli</a> to "
#~ "record?"
#~ msgstr ""
#~ "您知道您可以使用 <a href=\"help:sysprof\">sysprof-cli</a> 来进行记录吗？"

#~ msgid "Events"
#~ msgstr "事件"

#~ msgid "Select for more details"
#~ msgstr "选择更多详细信息"

#~ msgid "Open a perf event stream"
#~ msgstr "打开 perf 事件流"

#~ msgid "Authentication is required to access system performance counters."
#~ msgstr "需要授权才能访问系统性能计数器。"

#~ msgid "Get a list of kernel symbols and their address"
#~ msgstr "获取内核符号及其地址的列表"

#~ msgid "Authentication is required to access Linux kernel information."
#~ msgstr "需要授权才能访问 Linux 内核信息。"

#~ msgctxt "shortcut window"
#~ msgid "Sysprof Shortcuts"
#~ msgstr "Sysprof 快捷键"

#~ msgctxt "shortcut window"
#~ msgid "Save Recording"
#~ msgstr "保存记录"

#~ msgctxt "shortcut window"
#~ msgid "Saves the current recording"
#~ msgstr "保存当前记录"

#~ msgctxt "shortcut window"
#~ msgid "Opens a previously saved recording"
#~ msgstr "打开以前保存的记录"

#~ msgctxt "shortcut window"
#~ msgid "Recording"
#~ msgstr "记录"

#~ msgctxt "shortcut window"
#~ msgid "Record again"
#~ msgstr "再次记录"

#~ msgctxt "shortcut window"
#~ msgid "Starts a new recording"
#~ msgstr "开始新记录"

#~ msgctxt "shortcut window"
#~ msgid "Stop recording"
#~ msgstr "停止记录"

#~ msgctxt "shortcut window"
#~ msgid "Callgraph"
#~ msgstr "调用图"

#~ msgctxt "shortcut window"
#~ msgid "Expand function"
#~ msgstr "展开函数"

#~ msgctxt "shortcut window"
#~ msgid "Shows the direct descendants of the callgraph function"
#~ msgstr "在调用图中显示函数的直属后代"

#~ msgctxt "shortcut window"
#~ msgid "Collapse function"
#~ msgstr "折叠函数"

#~ msgctxt "shortcut window"
#~ msgid "Hides all callgraph descendants below the selected function"
#~ msgstr "在调用图中隐藏选定的函数下方的全部后代"

#~ msgctxt "shortcut window"
#~ msgid "Jump into function"
#~ msgstr "跳转到函数内"

#~ msgctxt "shortcut window"
#~ msgid "Selects the function or file as the top of the callgraph"
#~ msgstr "选择函数或文件作为调用图的顶端"

#~ msgctxt "shortcut window"
#~ msgid "Visualizers"
#~ msgstr "可视化工具"

#~ msgid "New Tab"
#~ msgstr "新建标签页"

#~ msgid "Save Recording…"
#~ msgstr "保存记录…"

#~ msgid "Open Capture…"
#~ msgstr "打开采集文件…"

#~ msgid "All Files"
#~ msgstr "全部文件"

#~ msgid "_Open"
#~ msgstr "打开 (_O)"

#~ msgid "Open Recording… (Ctrl+O)"
#~ msgstr "打开记录… (Ctrl+O)"

#~ msgid "Stopping profiler. Press twice more ^C to force exit."
#~ msgstr "正在停止性能分析器。再按两次 ^C 强制退出。"

#~ msgid "Profiler stopped."
#~ msgstr "性能分析器已停止。"

#~ msgid "Connect to the system bus"
#~ msgstr "连接到系统总线"

#~ msgid "Connect to the given D-Bus address"
#~ msgstr "连接到给定的 D-Bus 地址"

#~ msgid "Destination D-Bus name to invoke method on"
#~ msgstr "要在其上调用方法的目标 D-Bus 名称"

#~ msgid "Object path to invoke method on"
#~ msgstr "要在其上调用方法的对象路径"

#~ msgid "/org/gnome/Sysprof3/Profiler"
#~ msgstr "/org/gnome/Sysprof3/Profiler"

#~ msgid "Timeout in seconds"
#~ msgstr "超时秒数"

#~ msgid "Overwrite FILENAME if it exists"
#~ msgstr "若文件名存在则覆盖"

#~ msgid "--dest=BUS_NAME [FILENAME] - connect to an embedded sysprof profiler"
#~ msgstr "--dest=总线名称 [文件名] - 连接到嵌入的 sysprof 性能分析器"

#~ msgid "Last Spawn Program"
#~ msgstr "上一次生成的程序"

#~ msgid ""
#~ "The last spawned program, which will be set in the UI upon restart of the "
#~ "application."
#~ msgstr "上一次生成的程序，会被设置于重新启动应用程序后的用户界面中。"

#~ msgid "Last Spawn Inherit Environment"
#~ msgstr "上一次生成继承环境"

#~ msgid "If the last spawned environment inherits the parent environment."
#~ msgstr "上一次生成的环境是否继承上层环境。"

#~ msgid "Last Spawn Environment"
#~ msgstr "上一次生成的环境"

#~ msgid ""
#~ "The last spawned environment, which will be set in the UI upon restart of "
#~ "the application."
#~ msgstr "上一次生成的环境，会被设置于重新启动应用程序后的用户界面中。"

#~ msgid "Filename"
#~ msgstr "文件名"

#~ msgid "Samples Captured"
#~ msgstr "已采集的样本"

#~ msgid "Forks Captured"
#~ msgstr "已采集的分支"

#~ msgid "New variable…"
#~ msgstr "新建变量…"

#~ msgid "Profilers"
#~ msgstr "性能分析器"

#~ msgid "All Processes"
#~ msgstr "全部进程"

#~ msgid ""
#~ "Include all applications and operating system kernel in callgraph. This "
#~ "may not be possible on some system configurations."
#~ msgstr ""
#~ "在调用图中包含全部应用程序和操作系统内核。这在某些系统配置上可能无法实现。"

#~ msgid "Enable to launch a program of your choosing before profiling."
#~ msgstr "启用此选项以便在性能分析前启动您选择的程序。"

#~ msgid "Environment"
#~ msgstr "环境"

#~ msgid ""
#~ "If disabled, your CPU will be placed in performance mode. It will be "
#~ "restored after profiling."
#~ msgstr "如果禁用此选项，您的 CPU 会被置于性能模式。在性能分析完成后会恢复。"

#~ msgid "Learn more about Sysprof"
#~ msgstr "了解关于 Sysprof 的更多信息"

#~ msgid "Window size (width and height)."
#~ msgstr "窗口大小（宽度和高度）。"

#~ msgid "Window position"
#~ msgstr "窗口位置"

#~ msgid "Window position (x and y)."
#~ msgstr "窗口位置（x 和 y）。"

#~ msgid "Window maximized"
#~ msgstr "窗口最大化"

#~ msgid "Window maximized state"
#~ msgstr "窗口最大化状态"
